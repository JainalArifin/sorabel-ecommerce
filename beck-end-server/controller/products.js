var Products = require("../models/Products");

module.exports = {
  get: (req, res) => {
    Products.find((err, data) => {
      // Note that this error doesn't mean nothing was found,
      // it means the database had an error while searching, hence the 500 status
      if (err) return res.status(500).send(err)
      // send the list of all people
      return res.status(200).send(data);
    })
  },
  findOne: (req, res) => {
    Products.findOne({ _id: req.params.id },(err, data) => {
      // Note that this error doesn't mean nothing was found,
      // it means the database had an error while searching, hence the 500 status
      if (err) return res.status(500).send(err)
      // send the list of all people
      return res.status(200).send(data);
    })
  },
  post: (req, res) => {
    const newProduct = new Products(req.body)
    newProduct.save()
      .then((data) => {
        res.send(data)
      })
      .catch((err) => {
        res.send(err)
      })
  },
  update: (req, res) => {
   
    Products.findOneAndUpdate({ _id: req.params.id }, { $set: req.body }, { new: true }).then((docs) => {
      if (docs) {
        res.send({ success: true, data: docs });
      } else {
        res.send({ success: false, data: "no such user exist" });
      }
    }).catch((err) => {
      res.send(err);
    })
  },
  delete: (req, res)=>{
    Products.remove({_id: req.params.id})
    .then((docs)=>{
      if(docs) {
        res.send({"success":true,data:docs});
      } else {
        res.send({"success":false,data:"no such user exist"});
      }
    }).catch((err)=>{
      res.send(err);
    })
  }
}