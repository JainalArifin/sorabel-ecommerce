const Admin = require("../models/Admin");
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
require('dotenv').config()


module.exports = {
  get: (req, res) => {
    Admin.find((err, data) => {
      // Note that this error doesn't mean nothing was found,
      // it means the database had an error while searching, hence the 500 status
      if (err) return res.status(500).send(err)
      // send the list of all people
      return res.status(200).send(data);
    })
  },
  signup: (req, res) => {
    Admin.find({ username: req.body.username })
    .then((user)=>{
      if(user.length >= 1){
        return res.status(409).json({
          message: 'Username exists'
        })
      }else{
        bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
          Admin.create({
            username: req.body.username,
            password: hash
          })
            .then((data) => {
              res.status(200).send(data)
            })
            .catch((err) => {
              res.status(500).json({
                error: err
              })
            })
        })

      }
    })
    
  },
  signin: (req, res) => {
    Admin.findOne({
       username: req.body.username
    }).then((user) => {
      // console.log("=== user =>", user)
      if (!user) {
        // console.log("=== 1")
        res.status(404).json({
          message: 'username not found'
        })
      } else {
        console.log("=== masuk 1")
        bcrypt.compare(req.body.password, user.password, (err, result) => {
          if(err){
            console.log("=== masuk error")
            return res.status(401).send({
              message: 'Auth failed'
            })
          }
          if (result == true) {
            const token = jwt.sign({
              username: user.username,
              userId: user._id
            }, 
            process.env.JWT_PASS,
            {
              expiresIn: '10h'
            })
            
            // console.log("=== masuk true", token)
            return res.status(200).send({
              message: "Auth successful",
              token: token
            })
          } 

          res.status(401).send({
            message: 'Auth failed'
          })

        })
      }
    })
    .catch((err)=>{
      res.send(err)
    })
  },
  update: (req, res) => {

    Admin.findOneAndUpdate({ _id: req.params.id }, { $set: req.body }, { new: true }).then((docs) => {
      if (docs) {
        res.send({ success: true, data: docs });
      } else {
        res.send({ success: false, data: "no such user exist" });
      }
    }).catch((err) => {
      res.send(err);
    })
  },
  delete: (req, res) => {
    Admin.remove({ _id: req.params.id })
      .then((docs) => {
        if (docs) {
          res.send({ "success": true, data: docs });
        } else {
          res.send({ "success": false, data: "no such user exist" });
        }
      }).catch((err) => {
        res.send(err);
      })
  }
}