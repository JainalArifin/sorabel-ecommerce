var express = require('express');
var router = express.Router();
var product = require('../controller/products') 

router.get('', product.get)
router.get('/:id', product.findOne)
router.post('', product.post)
router.put('/:id', product.update)
router.delete('/:id',  product.delete)

module.exports = router;