var express = require('express');
var router = express.Router();
var admin = require('../controller/admin') 

router.get('', admin.get)
router.post('/signup', admin.signup)
router.post('/signin', admin.signin)
router.put('/:id', admin.update)
router.delete('/:id',  admin.delete)

module.exports = router;