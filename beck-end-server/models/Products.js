var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var products = new Schema({
  title: String,
  photo: String,
  price: Number,
  bahan: [],
  other: [
    {
      size: [
        {
          s:[],
          m:[],
          l:[],
          xl:[],
        }
      ],
      color: []
    }
  ],
  bisaCobaDanKembalikan: Boolean
});

var Products = mongoose.model('products', products);

module.exports = Products