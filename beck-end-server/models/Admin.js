var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var admin = new Schema({
  username: String,
  password: String
});

var Admin = mongoose.model('admin', admin);

module.exports = Admin;