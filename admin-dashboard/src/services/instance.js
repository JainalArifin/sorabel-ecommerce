import axios from 'axios';
import api from '../config/api'

const instance = axios.create({
  baseURL: api
})

export default instance;