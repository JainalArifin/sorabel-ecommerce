import instance from './instance';

const Get = (path, params) => {
  // console.log("====== 1 ==>")
  const promise = new Promise((resolve, reject) => {

  // console.log("====== 2 ==>")
    instance.get(`/${path}/${params ? params : ''}`)
    .then(({ data }) => {
      // console.log("====== 3 ==>")
      resolve(data)
    },(err) => {
      reject(err)
    })
  })
  return promise
}


const Post = (path, data) => {
  // console.log("====== 1 ==>")
  const promise = new Promise((resolve, reject) => {

  // console.log("====== 2 ==>")
    instance.post(`/${path}`, { data })
    .then(({ data }) => {
      // console.log("====== 3 ==>")
      resolve(data)
    },(err) => {
      reject(err)
    })
  })
  return promise
}


const Delete = (path, params) => {
  // console.log("====== 1 ==>")
  const promise = new Promise((resolve, reject) => {

  // console.log("====== 2 ==>")
    instance.delete(`/${path}/${params}`)
    .then(({ data }) => {
      // console.log("====== 3 ==>")
      resolve(data)
    },(err) => {
      reject(err)
    })
  })
  return promise
}

const getProduct = (path, params) => Get(path, params)
const postProduct = (path, data) => Post(path, data)
const deleteProduct = (path, params) => Delete(path, params)

const services = {
  getProduct,
  postProduct,
  deleteProduct
}

export default services