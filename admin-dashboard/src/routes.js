import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Products from './pages/products/Products';
import AddProduct from './pages/products/AddProduct';
import EditProduct from './pages/products/EditProduct';
import Login from './pages/login/Login';


const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      localStorage.getItem('new-token') ? (
        <Component {...props} />
      ) : (
        <Redirect 
          to={{
            pathname: "/login",
            state : { from: props.location}
          }}
        />
      )
    }
  />
)

export default class Routes extends Component {
  render(){
    return (
      <div>
          <Switch>
            <PrivateRoute exact path="/" component={Products} />
            <PrivateRoute path="/add" component={AddProduct} />
            <PrivateRoute path="/product/:id" component={EditProduct} />
            <Route path="/login" component={Login} />
          </Switch>
      </div>
    )
  }
}