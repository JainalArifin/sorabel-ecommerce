import React, { Component } from 'react';
import {
 Row, Col,
 Form, Input, Button
} from 'reactstrap';
import axios from 'axios';
import imgAdmin from '../../assets/images/user.png'
import api from '../../config/api';

import { Redirect } from 'react-router-dom';

import './style.css';

class Login extends Component {
  state = {
    username: '',
    password: '',
    isLoad: false
  }
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  handleLogin = () => {
    axios.post(`${api}admin/signin`, {
      username: this.state.username,
      password: this.state.password
    })
    .then(({ data })=>{
      // console.log(data.token)
      localStorage.setItem("new-token", data.token)
      this.setState({ isLoad: true })

    })
  }
  render() { 
    const { username, password, isLoad } = this.state

    if(isLoad === true){
      return <Redirect to="/" />
    }
    return (
      <div>
        <Row>
          <Col md={6} className="left  d-flex flex-column justify-content-end">
            <div className="title-admin text-center">
               Admin sorabel
            </div>
          </Col>
          <Col md={6} className="right  d-flex justify-content-center">
            <div className="text-center">
              <img src={imgAdmin} alt="admin" className="amin-login" />
              <div className="mt-5">
                <h6 className="text-input">username</h6>
                <Input type="text" 
                  name="username" 
                  value={username}
                  onChange={this.handleChange}
                  style={{
                    width: '400px',
                    borderRadius: '30px',
                    borderColor: '#AC145A',
                    height: '50px',
                    marginBottom: '40px'
                  }}
                  placeholder="Eg. demo"

                />
                <h6 className="text-input">password</h6>
                <Input type="password" 
                  name="password" 
                  value={password}
                  onChange={this.handleChange}
                  style={{
                    width: '400px',
                    borderRadius: '30px',
                    borderColor: '#AC145A',
                    height: '50px'
                  }}
                  placeholder="Eg. 123456"
                />
                <Button
                  color="danger"
                  className="mt-5 btn-login"
                  onClick={() => this.handleLogin()}
                >Login</Button>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
 
export default Login;