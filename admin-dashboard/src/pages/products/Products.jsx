import React, { Component } from 'react';
import api from '../../services'
import {
  Table, Button, Container, Card,
  Spinner
} from 'reactstrap';
import convertPrice from '../../utils/convertRupiah';
import { Link } from 'react-router-dom';

import './style.css';

class Products extends Component {
  state = {
    products: [],
    isLoad: false
  }
  getData = () => {
    api.getProduct('products')
      .then((data) => {
        // console.log("== data =>", data)
        this.setState({ products: data, isLoad: false })
      })
  }

  componentWillMount() {
    this.getData()
  }

  handleDelete = (id) => {
    this.setState({ isLoad: true })
    api.deleteProduct('products', id)
      .then((data) => {
        // this.setState({ isLoad: false })
        this.getData()
      })
  }
  render() {
    const { products, isLoad } = this.state;
    return (
      <Container className="mt-4">
        <Link to="/add">
          <Button color="primary">Add Product</Button>
        </Link>
        <Card className="card-product">
          <h4>List Products</h4>
          <Table>
            <thead>
              <tr>
                <th>#</th>
                <th>Photo</th>
                <th>Title</th>
                <th>Price</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {products.map((list, idx) => {
                return (
                  <tr key={idx}>
                    <td>{idx + 1}</td>
                    <td>
                      <img src={list.photo[0]} alt={list.title} className="card-photo" />
                    </td>
                    <td>{list.title}</td>
                    <td>
                      {list.price !== undefined && (
                        convertPrice(list.price)
                      )}
                    </td>
                    <td>
                      <Link to={`/product/${list._id}`}>
                        <Button
                          style={{ width: '80px' }}
                          color="primary"
                        >Edit</Button>{' '}
                      </Link>
                      <Button
                        style={{ width: '80px' }}
                        color="danger"
                        onClick={() => this.handleDelete(list._id)}
                      >
                        Delete
                      </Button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </Table>
        </Card>
      </Container>
    );
  }
}

export default Products;