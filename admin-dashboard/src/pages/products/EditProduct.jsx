import React, { Component } from 'react';
import {
  Container,
  Card,
  Row, Col,
  Button, Form, FormGroup, Label, Input, FormText
} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import api from '../../config/api'
import services from '../../services'
 
class EditProduct extends Component {
  state = {
    title: '',
    price: 0,
    bahan: '',
    size: '',
    color: '',
    linkImage: '',
    redirect: false
  }

  getParams = () => {
    // console.log("=== ok =>", this.props.match.params.id)
    services.getProduct('products', this.props.match.params.id)
    .then((data)=>{
      this.setState({
        title: data.title,
        price: data.price,
        bahan: data.bahan === undefined ? '' :  data.bahan.toString(),
        size: data.size === undefined ? '' : data.bahan.toString(),
        color: data.color === undefined ? '' : data.color.toString(),
        linkImage:  data.photo === undefined ? '' : data.photo.toString(),
      })
    })
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    const {
      title, price, bahan, size, color, linkImage
    } = this.state;

    axios.put(`${api}products/${this.props.match.params.id}`, {
      title: title,
      price: price,
      bahan: bahan.split(','),
      size: size.split(','),
      color: color.split(','),
      photo: linkImage.split(',')
    })
    .then((data) => {
          this.setState({
            title: '',
            price: '',
            bahan: '',
            size: '',
            color: '',
            linkImage: '',
            redirect: true
          })
        })
  }

  componentWillMount(){
    this.getParams()
  }


  render() { 
    const {
      title, price, bahan, size, color, linkImage, redirect
    } = this.state;

    if (redirect) {
      return <Redirect to="/" />
    }
    return (
      <Container>
        <Card className="card-product">
          <Form onSubmit={this.handleSubmit}>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label for="title">Title</Label>
                  <Input type="text" name="title" id="title" placeholder="Eg. Adeia Flowery Twist Mini Dress"
                    onChange={this.handleChange}
                    value={title}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="price">Price</Label>
                  <Input type="number" name="price" id="price" placeholder="Eg. 149000"
                    onChange={this.handleChange}
                    value={price}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label for="bahan">Bahan</Label>
                  <Input type="text" name="bahan" id="bahan" placeholder="Eg. Katun, Crepe kombinasi Baloteli"
                    onChange={this.handleChange}
                    value={bahan}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="size">Size</Label>
                  <Input type="text" name="size" id="size" placeholder="Eg. s, m, l, xl"
                    onChange={this.handleChange}
                    value={size}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label for="color">Color</Label>
                  <Input type="text" name="color" id="color" placeholder="Eg. Katun, Crepe kombinasi Baloteli"
                    onChange={this.handleChange}
                    value={color}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="photo">Link Image</Label>
                  <Input type="text" name="linkImage" id="photo" placeholder="Eg. https://imager-next.freetls.fastly.net/images/resized/480/d30dd206-70ab-4814-aa0c-68c1711ce749"
                    onChange={this.handleChange}
                    value={linkImage}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Button color="primary">Update Product</Button>{' '}
            <Link to="/">
              <Button color="danger">Cancel</Button>
            </Link>
          </Form>
        </Card>
      </Container>
    );
  }
}
 
export default EditProduct;