import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom'
import Catalog from './pages/catalog/Catalog';
import Home from './layouts/Home';
import DetailProduct from './pages/detail-product/DetailProduct';
import Keranjang from './pages/keranjang/Keranjang';

class Routes extends Component {
  render() {
    return (
      <>
        <Home>
          <Switch>
            <Route exact path="/" render={() => (<Redirect to="/catalog" />)} />
            <Route exact path="/catalog" component={Catalog} />
            <Route path="/catalog/:id" component={DetailProduct} />
            <Route path="/cart" component={Keranjang} />
          </Switch>
        </Home>
      </>
    );
  }
}

export default Routes;