const convertRupiah = (bilangan) => {
  if (bilangan !== "-") {
    var number_string = bilangan.toString()
    var sisa = number_string.length % 3
    var rupiah = number_string.substr(0, sisa)
    var ribuan = number_string.substr(sisa).match(/\d{3}/g)
    let separator
    if (ribuan) {
      separator = sisa ? '.' : ''
      rupiah += separator + ribuan.join('.')
    }
    return rupiah
  } else {
    return '-'
  }
}

export default convertRupiah;