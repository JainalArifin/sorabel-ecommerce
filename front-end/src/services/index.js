import instance from './instance';

const Get = (path, pathLink) => {
  // console.log("====== 1 ==>")
  const promise = new Promise((resolve, reject) => {

  // console.log("====== 2 ==>")
    instance.get(`/${path}`)
    .then(({ data }) => {
      // console.log("====== 3 ==>")
      resolve(data)
    },(err) => {
      reject(err)
    })
  })
  return promise
}

const getCatalog = (path) => Get(path)

const services = {
  getCatalog
}

export default services