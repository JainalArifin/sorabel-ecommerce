import React from 'react';
// import Header from '../components/header/Header';
// import Footer from '../components/footer/Footer';

import './style.css';

const Home = (props) => {
  return (
    <div className="home">
      {/* ====== start of main content */}
      <div className="size-mobile">
        {props.children}
      </div>
      {/* ====== end of main content */}
    </div>
  );
}


export default Home;