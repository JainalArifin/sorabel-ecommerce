import React, { Component } from 'react';
import './style.css'
import cart from '../../assets/images/shopping-basket-button.png';
import {
  Badge
} from 'reactstrap';

import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class Header extends Component {
  render(){
    return (
      <div className="header fixed-top">
        <div className="size-mobile">
          <div className="d-flex justify-content-between logo-fix align-items-center">
            <div>
              {this.props.children}
            </div>
            <Link to="/cart">
              <div>
                <img src={cart} alt="cart" className="cart" />
                {this.props.cartData.length > 0 && (
                  <Badge className="badge-chart">{this.props.cartData.length}</Badge>
                )}
              </div>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cartData: state.CART
  }
}
const mapDispatchToProps = () => {

}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);