import React, { Component } from 'react';

import './style.scss'

class Skelton extends Component {
  render() {
    return (
      <div>
        <section className="card">
          <figure className="card-image loading"></figure>
          <div className="card-detail">
            <h3 className="card-title loading"></h3>
            <p className="card-description loading"></p>
          </div>
        </section>
      </div>
    );
  }
}

export default Skelton;