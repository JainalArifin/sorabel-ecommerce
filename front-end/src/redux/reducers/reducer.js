import {
  ADD_CART, BAYAR, REMOVE_CART
} from '../actionTypes';

const initialState = {
  CART: []
}

function counter(state = initialState, action) {

  switch (action.type) {
    case ADD_CART:
      return {
        ...state,
        CART: [...state.CART, action.payload],
      }
    case BAYAR:
      return {
        ...state,
        CART: [],
      }
    case REMOVE_CART:
      state.CART.splice(action.payload, 1)
      console.log("=== > 1",  state.CART)
      return {
        CART: state.CART
      }
    default:
      return state
  }
}

export default counter;