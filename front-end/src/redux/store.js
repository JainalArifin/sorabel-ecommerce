import { createStore } from 'redux';
import reducer from './reducers/reducer';
import { devToolsEnhancer } from 'redux-devtools-extension';

let store = createStore(reducer
  ,devToolsEnhancer()
);

export default store