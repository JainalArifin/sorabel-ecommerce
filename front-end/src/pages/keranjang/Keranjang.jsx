import React, { Component } from 'react';
import Header from '../../components/header/Header';
import backBtn from '../../assets/images/icon-back.png'
import { Link } from 'react-router-dom';
import {
  Badge, Button,
  Modal, Row, Col, ModalBody
} from 'reactstrap';
import convertRupiah from '../../utils/convertRupiah';
import Swal from 'sweetalert2';

// redux
import { connect } from 'react-redux';
import { BAYAR, REMOVE_CART } from '../../redux/actionTypes';
// routing
import { Redirect } from 'react-router-dom'

// ===> image
import closeCart from '../../assets/images/icon-close.png';
import imgGuest from '../../assets/images/img-guest-checkout.png'
import imgLeh from '../../assets/images/img-leh.png'

import './style.css';

class Keranjang extends Component {
  state = {
    modal: false,
    redirect: false,
    idx: ''
  }

  toggle = (idx) => {
    this.setState(prevState => ({
      modal: !prevState.modal,
      idx: idx
    }));

  }
  handleBayar = () => {
    this.props.bayar()
    Swal.fire({
      type: 'success',
      title: 'Barang anda sedang kami proses',
      showConfirmButton: false,
      timer: 2000
    })
    this.setState({ redirect: true })

  }
  handleDelete = () => {
    // console.log("==== product haspus =>", this.state.idx)
    this.props.removeCart(this.state.idx)
    this.setState({
      modal: false
    })
  }
  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />
    }
    return (
      <div>
        <Header>
          <div className="d-flex align-items-center">
            <div>
              <Link to="/">
                <img src={backBtn} alt="back" className="back-btn" />
              </Link>
            </div>
            <div className="title-back">Keranjang Belanja</div>
          </div>
        </Header>

        {this.props.cartData.length === 0 ?
          <div className="cart-belanjaan text-center">
            <img src={imgGuest} alt="img-kosong" className="img-kosong" />
            <h4>Keranjang Masih Kosong</h4>
            <h6>Ayo diborong Sis, sebelum kehabisan.</h6>
            <h6>ongkos kirim GRATIS lho!</h6>
          </div>
          :
          <>
            <div className="cart-belanjaan">
              <div className="title-cart">
                TOTAL {this.props.cartData.length} BARANG
              </div>
              {this.props.cartData.map((list, idx) => {
                return (
                  <div className="d-flex justify-content-between mt-3">
                    <div>
                      <img src={list.photo[0]} alt={list.title} className="cart-photo" />
                    </div>

                    <div className="d-flex belanjaan justify-content-between">
                      <div className="ml-2">
                        <Badge color="success"
                          className="badge-coba"
                        >Coba Dulu</Badge>
                        <div className="title-cart">{list.title.substring(0, 15)}...</div>
                      </div>

                      <div>
                        <img src={closeCart} alt="close" className="btn-close"
                          onClick={() => this.toggle(idx)}
                        />
                        <div className="price">{convertRupiah(list.price)}</div>
                      </div>

                    </div>
                  </div>
                )
              })}
              <hr />
              <div className="d-flex justify-content-between">
                <div>Subtotal</div>
                <div className="price">
                  {convertRupiah(this.props.cartData.reduce((sum, i) => sum + i.price, 0))}
                </div>
              </div>
            </div>
            <div className="card-page">
              <div className="title-cart">
                ONGKIR
              </div>
              <div className="d-flex justify-content-between">
                <div>
                  Kirim ke ...
                </div>
                <div className="gratis">
                  Gratis
                </div>
              </div>
            </div>

            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
              <ModalBody>
                <div className="text-center">
                  <img src={imgLeh} alt="img-kosong" className="img-kosong" />
                  <h6>Barang ini akan dihapus dari keranjang sista. Simpan aja ke wishlist supaya besok bisa sista lihat lagi Sis :)</h6>
                </div>
                <Row>
                  <Col>
                    <Button
                      onClick={() => this.handleDelete()}
                      outline color="secondary"
                      block
                    >HAPUS</Button>

                  </Col>
                  <Col>
                    <Button
                      onClick={this.toggle}
                      color="secondary"
                      block
                      className="btn-beli"
                    >BATAL</Button>
                  </Col>
                </Row>
              </ModalBody>
            </Modal>
            <div className="card-page bottom-card">
              <Button className="btn-beli" block
                onClick={() => this.handleBayar()}
              >Bayar</Button>
            </div>

          </>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cartData: state.CART
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    bayar: () => dispatch({ type: BAYAR }),
    removeCart: (payload) => dispatch({ type: REMOVE_CART, payload: payload })
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Keranjang);