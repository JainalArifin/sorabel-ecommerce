import React, { Component } from 'react';
import api from '../../services/index'
import {
  CardImg,
  CardBody,
  Button
} from 'reactstrap';
import convertRupiah from '../../utils/convertRupiah'
import { Link } from 'react-router-dom'

import './style.css';
import Skelton from '../../components/skeleton/Skeleton';
import Header from '../../components/header/Header';
import logo from '../../assets/images/logo-sorabel.png';

import { connect } from 'react-redux';
import { ADD_CART } from '../../redux/actionTypes';

class Catalog extends Component {
  state = {
    products: [],
    isLoad: false
  }
  getData = () => {
    window.scrollTo(0, 0)
    this.setState({ isLoad: true })
    api.getCatalog('products')
      .then((data) => {
        // console.log("=== data new =>", data)
        this.setState({
          products: data,
          isLoad: false
        })
      })
      .catch((err) => { console.error(err) })
  }
  componentDidMount() {
    this.getData()
  }
  handleBeli = (list) => {
    // console.log("=== cart =>", typeof cart)
    // console.log("=== list =>", typeof list)
    // let cart = JSON.stringify([list])
    // localStorage.setItem('cart', cart)

  }
  render() {
    const { products, isLoad } = this.state;
    return (
      <div>
        <Header>
          <Link to="/catalog">
            <img src={logo} alt="logo" className="logo"/>
          </Link>
        </Header>
        <div className="catalog">
          {/* skeltaon */}
          {isLoad ? 
            <>
              <Skelton />
              <Skelton />
            </>
            :
            products.map((list, idx) => {
              return (
                <div key={idx}>
                  <div className="p-0 bg-white">
                    <Link to={`/catalog/${list._id}`} className="link">
                      <CardImg top width="100%" src={list.photo[0]} alt="Card image cap" />
                    </Link>
                    <CardBody>
                      <div className="d-flex justify-content-between">
                        <div>
                          {/* {console.log("===== id=", list._id)} */}
                          <Link to={`/catalog/${list._id}`} className="title">
                            <div>{list.title.substr(0, 24)}...</div>
                          </Link>
                          <div className="size-clothes d-flex justify-content-center">
                            {/* {list.other[0].size.map((item, idxSize)=>(
                                <span className="size-sub" key={idxSize}>{Object.keys(item)}</span>
                              ))} */}
                            <span className="size-sub" >S,{' '}</span>
                            <span className="size-sub" >M,{' '}</span>
                            <span className="size-sub" >L,{' '}</span>
                            <span className="size-sub" >XL</span>
                          </div>
                          <div className="price">
                            {convertRupiah(list.price)}
                          </div>
                        </div>
                        <div>
                          <Button className="btn-beli"
                            // onClick={() => this.handleBeli(list)}
                            onClick={() => this.props.addCart(list) }
                          >BELI</Button>
                        </div>
                      </div>
                    </CardBody>
                  </div>
                </div>
              )
            })
          }


        </div>
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {}
}
const mapDispatchToProps = (dispatch) => {
  return {
    addCart: ( payload ) => dispatch({ type: ADD_CART, payload: payload })
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Catalog);