import React, { Component } from 'react';
import api from '../../services/index'
import {
  CardImg,
  CardBody,
  Button
} from 'reactstrap';
import convertRupiah from '../../utils/convertRupiah'

import './style.css';
import Skelton from '../../components/skeleton/Skeleton';

class OtherProducts extends Component {
  state = {
    products: [],
    isLoad: false
  }
  getData = () => {
    this.setState({ isLoad: true })
    api.getCatalog('products?_order=desc')
      .then((data) => {
        console.log("=== data new =>", data)
        this.setState({
          products: data,
          isLoad: false
        })
      })
      .catch((err) => { console.error(err) })
  }
  componentDidMount() {
    window.scrollTo(0, 0)
    this.getData()
  }
  render() {
    const { products, isLoad } = this.state;
    return (
      <div>
        {isLoad ?
          <Skelton />
          :

          <div className="mt-2 bg-white">

            <h5 className="p-2">Rekomendasi Produk Lainnya</h5>
            {products.map((list, idx) => {
              return (
                <div key={idx}>
                  <div className="p-0 bg-white">
                    <CardImg top width="100%" src={list.photo[0]} alt="Card image cap" onClick={() => this.props.refreshNew(list._id)} style={{ cursor: 'pointer' }} />
                    <CardBody>
                      <div className="d-flex justify-content-between">
                        <div>
                          <div onClick={() => this.props.refreshNew(list._id)} style={{ cursor: 'pointer' }}>{list.title.substr(0, 24)}...</div>
                          <div className="size-clothes d-flex justify-content-center">
                            <span className="size-sub" >S,{' '}</span>
                            <span className="size-sub" >M,{' '}</span>
                            <span className="size-sub" >L,{' '}</span>
                            <span className="size-sub" >XL</span>
                          </div>
                          <div className="price">
                            {convertRupiah(list.price)}
                          </div>
                        </div>
                        <div>
                          <Button className="btn-beli"
                            onClick={()=>this.props.addCart(list)}
                          >BELI</Button>
                        </div>
                      </div>
                    </CardBody>
                  </div>
                </div>
              )
            })}
          </div>
        }
      </div>
    );
  }
}

export default OtherProducts;