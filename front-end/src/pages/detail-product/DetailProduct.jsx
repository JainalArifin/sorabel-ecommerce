import React, { Component } from 'react';
import api from '../../services/index'
import {
  // CardImg,
  CardBody,
  Button
} from 'reactstrap';
import convertRupiah from '../../utils/convertRupiah'

import './style.css'
// import Catalog from '../catalog/Catalog';
import OtherProducts from './OtherProducts';
import Skelton from '../../components/skeleton/Skeleton';
import Header from '../../components/header/Header'
import backBtn from '../../assets/images/icon-back.png'


import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

import { Link } from 'react-router-dom';


import { connect } from 'react-redux';
import { ADD_CART } from '../../redux/actionTypes';

class DetailProduct extends Component {
  state = {
    product: {},
    isload: false
  }
  getData = () => {
    window.scrollTo(0, 0)
    const { match } = this.props;
    this.setState({ isload: true })
    api.getCatalog(`products/${match.params.id}`)
    .then((data) => {
      console.log("=== new data =>", data)
      this.setState({
        product: data,
        isload: false
      })
    })
  }
  
  
  
  componentWillMount() {
    window.scrollTo(0, 0)
    this.getData()
  }
  
  
  updateData=(id)=> {
    window.scrollTo(0, 0)
    this.setState({ isload: true })
    api.getCatalog(`products/${id}`)
    .then((data) => {
      this.setState({
        product: data,
        isload: false
      })
    })
  } 
 
  render() {
    // console.log("=== cek props =>",this.props)
    const { product, isload } = this.state;
    return (
      <div>
        <Header>
          <div className="d-flex align-items-center">
            <div>
              <Link to="/">
                <img src={backBtn} alt="back" className="back-btn"/>
              </Link>
            </div>
            {product.title !== undefined && (
              <div className="title-back">{product.title.length >= 20 ? `${product.title.substring(0, 20)} ...` : product.title}</div>
            )}
          </div>
        </Header>
        {isload ?
          <>
            <Skelton />
          </>
          :
          <div className="catalog bg-white">
            <Carousel>
                {product.photo.map((list, idx)=>(
                <div key={idx}>
                    <img top width="100%" src={list} alt="Card image cap" />
                </div>
                ))}
            </Carousel>
            <CardBody>
              <div>{product.title}</div>
              <div className="price">
                {product.price !== undefined && (
                  convertRupiah(product.price)
                )}
              </div>
            </CardBody>
            <div className="others">
              <p>Warna pilihan</p>
              {product.other !== undefined && (
                product.other.map((list) => (
                  list.color.map((item) => (
                    <>
                      <Button outline >
                        {item}
                      </Button>{' '}
                    </>
                  ))
                ))
              )}
              <p>Pilih ukuran yang tersedia</p>
              {product.other !== undefined && (
                product.other.map((list) => (
                  list.size.map((item) => (
                    <>
                      <Button outline>
                        {Object.keys(item)}
                      </Button>{' '}
                    </>
                  ))
                ))
              )}
            </div>
            <div className="pt-3 pb-2 pl-2">
              <Button className="btn-beli"
                onClick={() => this.props.addCart(product) }
              >
                BELI SEKARANG
              </Button>
            </div>
          </div>
        }
        <OtherProducts 
          refreshNew={(id)=>this.updateData(id)} 
          addCart={(product) => this.props.addCart(product) }
        />
      </div>
    );
  }
}


const mapStateToProps = () => {
  return {}
}
const mapDispatchToProps = (dispatch) => {
  return {
    addCart: ( payload ) => dispatch({ type: ADD_CART, payload: payload })
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailProduct);